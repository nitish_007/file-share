import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FriendListComponent } from './components/friend-list/friend-list.component';
import { UserAuthGuardService } from 'src/app/auth-guard/user-auth-guard.service';
import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { TermsComponent } from './components/terms/terms.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'adminDashboard', canActivate : [ UserAuthGuardService ], component: AdminDashboardComponent },
  { path: 'userDashboard', canActivate : [ UserAuthGuardService ], component: UserDashboardComponent },
  { path: 'friendlist', canActivate : [ UserAuthGuardService ], component: FriendListComponent },
  { path: 'aboutus', canActivate : [ UserAuthGuardService ], component: AboutusComponent },
  { path: 'terms', canActivate : [ UserAuthGuardService ], component: TermsComponent },
  { path: 'fileupload', canActivate : [ UserAuthGuardService ], component: FileUploadComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
