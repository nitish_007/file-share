import { FriendService } from './../../services/friend.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css'],
})
export class FriendListComponent implements OnInit {
  friendList = [];
  searchText;
  loggedInUsername = '';
  username = '';

  constructor(private friendService: FriendService) {}

  ngOnInit(): void {
    this.loggedInUsername = localStorage.getItem('name');
    this.getFriendList();
  }

  getFriendList(): any {
    this.friendService.getFriendList().subscribe((data) => {
      this.friendList = data;
    });
  }
}
