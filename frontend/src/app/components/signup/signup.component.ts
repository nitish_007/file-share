import { UserService } from 'src/app/services/user.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  role;
  constructor(
    private userService: UserService,
    private toast: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userService._loginObservable.subscribe(() => {
      // this.isLoggedIn = this.userService.isLoggedIn();
      this.role = localStorage.getItem('role');
      if (this.role === 'admin') {
        this.router.navigate(['adminDashboard']);
      }
      if (this.role === 'user') {
        this.router.navigate(['userDashboard']);
      }
    });
  }

  onSubmit(form: any): any {
    form.invalid = true;
    const username = (form.elements.namedItem('username') as HTMLInputElement)
      .value;
    const email = (form.elements.namedItem('email') as HTMLInputElement).value;
    const password = (form.elements.namedItem('password') as HTMLInputElement)
      .value;
    const profile = (form.elements.namedItem('profile') as HTMLInputElement)
      .files[0];

    // console.log(profile);
    const values = {
      username,
      email,
      password,
      profile,
    };

    const data = new FormData();
    data.append('username', username);
    data.append('email', email);
    data.append('password', password);
    data.append('profile', profile);
    // console.log(data);

    this.userService.createUser(data).subscribe(() => {
      this.toast.success(
        'Verify your email to login',
        'User created successfully!!',
        {
          timeOut: 1500,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right',
        }
      );
      form.reset();
    },
    (error) => {
      this.toast.error(`${error.error.msg}`, 'Error', {
        timeOut: 2000,
        progressBar: true,
        progressAnimation: 'increasing',
        positionClass: 'toast-top-right',
      });
      form.reset();
    });
  }
}
