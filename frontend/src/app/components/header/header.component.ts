import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { FriendService } from 'src/app/services/friend.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  isLoggedIn = false;
  role;
  notifications = [];
  constructor(
    private userService: UserService,
    private router: Router,
    private friendService: FriendService,
    private toast: ToastrService,
  ) {}

  ngOnInit(): void {
    this.getRole();
    if (this.role === 'user') {
      this.getNotifications();
    }
    this.userService._loginObservable.subscribe(() => {
      this.role = localStorage.getItem('role');
      const token = this.userService.getToken();
      if (token !== '') {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
    // console.log(this.isLoggedIn);
  }

  getRole(): any {
    this.role = localStorage.getItem('role');
  }

  logout(): any {
    const userId = localStorage.getItem('id');
    this.userService.updateOnlineStatus(true, userId).subscribe();
    this.userService.logout();
    this.router.navigate(['']);
    this.isLoggedIn = this.userService.isLoggedIn();
    // console.log(this.isLoggedIn);
  }

  getNotifications(): any {
    this.friendService.getNotifications().subscribe((notifications) => {
      // console.log(notifications.incomingRequest);
      if (notifications.incomingRequest.length === 0) {
        notifications.incomingRequest.push({ msg: 'No New Notifications!!' });
      }
      this.notifications = notifications.incomingRequest;
    });
  }

  acceptRequest(id): any {
    this.friendService.acceptRequest(id).subscribe();
    this.friendService.getFriendList().subscribe();
    this.toast.success('Request accepted!!', 'Success', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-right',
    });
  }

  rejectRequest(id): any {
    this.friendService.rejectRequest(id).subscribe();
    this.friendService.getFriendList().subscribe();
    this.toast.error('Request rejected!!', 'Error', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-right',
    });
  }
}
