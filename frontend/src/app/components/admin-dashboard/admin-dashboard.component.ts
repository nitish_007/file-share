import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css'],
})
export class AdminDashboardComponent implements OnInit{
  users = [];
  activeUsers = [];
  username;
  searchText;
  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.getAllUsers();
    this.getActiveUsers();
    this.username = localStorage.getItem('name');
  }

  getAllUsers(): any {
    this.userService.getAllUsers().subscribe((users) => {
      this.users = users;
    });
  }

  getActiveUsers(): any {
    this.userService.getActiveUsers().subscribe((active) => {
      // console.log(active);
      this.activeUsers = active;
      // this.getActiveUsers();
    });
  }

  toggleBlockedState(blockState, id): any {
    blockState = !blockState;
    this.userService.updateBlockState(blockState, id).subscribe(() => {
      this.getAllUsers();
    });
  }

}
