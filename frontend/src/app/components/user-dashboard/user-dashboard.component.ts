import { Component, OnInit } from '@angular/core';
import { FriendService } from './../../services/friend.service';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css'],
})
export class UserDashboardComponent implements OnInit {
  users: [];
  searchText;
  loggedInUsername = '';
  profile = '';
  username = '';

  constructor(
    private userService: UserService,
    private friendService: FriendService,
    private toast: ToastrService
  ) {}

  ngOnInit(): void {
    this.getAllUsers();
    this.loggedInUsername = localStorage.getItem('name');
    this.profile = localStorage.getItem('profile');
  }

  getAllUsers(): any {
    this.userService.getAllUsers().subscribe((users) => {
      this.users = users;
    });
  }

  addToFriendList(id, username): any {
    this.username = username;
    this.friendService.addToFriendList(id).subscribe(
      (data) => {
        // console.log('success', data.msg);
        this.toast.success(`Friend request sent to ${username}`, 'Success', {
          timeOut: 2000,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right',
        });
      },
      (data) => {
        // console.log('Error', data.error.msg);
        this.toast.error(`${username} is already added !!`, 'Error', {
          timeOut: 2000,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right',
        });
      }
    );
  }
}
