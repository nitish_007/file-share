import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css'],
})
export class FileUploadComponent implements OnInit {
  multipleFiles: any = [];
  filesList: any = [];
  sharedWithMe: any = [];
  upload = true;
  fileId = '';
  modalRef: BsModalRef;

  constructor(
    private http: HttpClient,
    private toast: ToastrService,
    private modalService: BsModalService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.fetchUploadedFiles();
  }

  selectFiles(event): any {
    if (event.target.files.length > 0) {
      this.multipleFiles = event.target.files;
    } else {
      this.multipleFiles = [];
      this.toast.error('Please select a file', 'Error', {
        timeOut: 1500,
        progressBar: true,
        progressAnimation: 'increasing',
        positionClass: 'toast-top-right',
      });
    }
  }

  onSubmit(form): any {
    const formData = new FormData();
    if (this.multipleFiles.length < 1) {
      this.multipleFiles = [];
      return this.toast.error('Please select a file', 'Error', {
        timeOut: 1500,
        progressBar: true,
        progressAnimation: 'increasing',
        positionClass: 'toast-top-right',
      });
    }
    for (const file of this.multipleFiles) {
      formData.append('files', file);
    }
    form.reset();

    this.http
      .post<any>('http://127.0.0.1:3000/api/files/fileupload', formData)
      .subscribe(
        (res) => {
          this.multipleFiles = [];
          console.log(res);
          this.toast.success('Files uploaded successfully!!', 'Upload', {
            timeOut: 1500,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right',
          });
          this.fetchUploadedFiles();
        },
        (err) => console.log(err)
      );
  }

  fetchUploadedFiles(): any {
    this.http
      .get<any>('http://127.0.0.1:3000/api/files/getuploadedfile')
      .subscribe(
        (files) => {
          // console.log(files);
          this.filesList = files;
        },
        (err) => console.log(err)
      );
  }

  toggle(data: any): any {
    if (data === 'upload') {
      this.upload = true;
    } else {
      this.upload = false;
    }
  }

  // open model
  openModel(fileId, form): any {
    this.modalRef = this.modalService.show(form, {
      animated: true,
      class: 'modal-lg',
    });
    this.fileId = fileId;
  }

  shareFile(event: Event, form: HTMLFormElement): any {
    event.preventDefault();
    const email = (form.elements.namedItem('email') as HTMLInputElement).value;
    if (!email) {
      return this.toast.error('Email can\'t be empty', 'Error', {
        timeOut: 1500,
        progressBar: true,
        progressAnimation: 'increasing',
        positionClass: 'toast-top-right',
      });
    }
    this.toast.success('File shared successfully!!', 'Success', {
      timeOut: 2000,
      progressBar: true,
      progressAnimation: 'increasing',
      positionClass: 'toast-top-right',
    });
    this.userService.shareFile(this.fileId, { email }).subscribe();
    this.modalRef.hide();
    return false;
  }

  fetchSharedFile(): any {
    this.userService.fetchSharedFile().subscribe((data) => {
      this.sharedWithMe = data;
    });
  }

  closeModel(): any {
    this.modalRef.hide();
  }
}
