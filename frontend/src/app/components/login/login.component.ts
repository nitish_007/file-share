import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  isLoggedIn = false;
  role;
  userId;
  constructor(
    private userService: UserService,
    private router: Router,
    private toast: ToastrService
  ) {}

  ngOnInit(): void {
    this.userService._loginObservable.subscribe(() => {
      this.isLoggedIn = this.userService.isLoggedIn();
      if (this.isLoggedIn) {
        this.role = localStorage.getItem('role');
      }
      if (this.role === 'admin') {
        this.router.navigate(['adminDashboard']);
      }
      if (this.role === 'user') {
        this.router.navigate(['userDashboard']);
      }
    });
  }

  onSubmit(form: NgForm): any {
    // console.log(form.value);

    this.userService.login(form.value).subscribe(
      (result: any) => {
        this.isLoggedIn = this.userService.isLoggedIn();
        if (this.isLoggedIn) {
          this.userId = localStorage.getItem('id');
        }
        this.userService.updateOnlineStatus(true, this.userId).subscribe();
        if (result.role === 'admin' && result.blockedStatus === false) {
          this.router.navigate(['adminDashboard']);
          this.toast.success('Admin Dashboard', 'Welcome to,', {
            timeOut: 2000,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right',
          });
        } else if (result.role === 'user' && result.blockedStatus === false) {
          this.router.navigate(['userDashboard']);
          this.toast.success('User Dashboard', 'Welcome to,', {
            timeOut: 2000,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right',
          });
        } else {
          this.userService.logout();
          return this.toast.error('User is Blocked', 'Error', {
            timeOut: 2000,
            progressBar: true,
            progressAnimation: 'increasing',
            positionClass: 'toast-top-right',
          });
        }
      },
      (error) => {
        this.toast.error(`${error.error.msg}`, 'Error', {
          timeOut: 2000,
          progressBar: true,
          progressAnimation: 'increasing',
          positionClass: 'toast-top-right',
        });
      }
    );
    form.reset();
  }
}
