import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FriendService {
  friendUrl = 'http://127.0.0.1:3000/api/friends';

  constructor(private http: HttpClient) { }

  // sending friend request
  addToFriendList(id): any {
    return this.http.put(`${this.friendUrl}/addtofriendlist/${id}`, id);
  }

  // fetching friend list
  getFriendList(): any {
    return this.http.get(`${this.friendUrl}/getfriendlist`);
  }

  // fetch incoming friend request
  getNotifications(): any {
    return this.http.get(`${this.friendUrl}/notifications`);
  }

  // accepting friend request
  acceptRequest(id): any {
    return this.http.put(`${this.friendUrl}/acceptrequest/${id}`, id);
  }

  // rejecting friend request
  rejectRequest(id): any {
    return this.http.put(`${this.friendUrl}/rejectrequest/${id}`, id);
  }
}
