import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  userUrl = 'http://127.0.0.1:3000/api/users';
  loginObservable: BehaviorSubject<{}>;

  constructor(private http: HttpClient) {
    this.loginObservable = new BehaviorSubject({});
  }

  // Getter method used to subscribe data during login and logout
  public get _loginObservable(): any {
    return this.loginObservable;
  }

  // Getting token from localsorage
  getToken(): any {
    return localStorage.getItem('token') ? localStorage.getItem('token') : '';
  }
  // creating new user
  createUser(data: any): any {
    return this.http.post(`${this.userUrl}/create`, data);
  }

  // login user
  login(creds: any): any {
    return this.http.post(`${this.userUrl}/login`, creds).pipe(
      map((result: LoginResponse) => {
        localStorage.setItem('token', 'Bearer ' + result.token);
        localStorage.setItem('id', result.id);
        localStorage.setItem('role', result.role);
        localStorage.setItem('name', result.username);
        localStorage.setItem('profile', result.profile);
        this.getActiveUsers().subscribe();
        this.loginObservable.next({});
        return result;
      })
    );
  }

  // Checking user session
  isLoggedIn(): boolean {
    if (this.getToken() !== '') {
      return true;
    }
    return false;
  }

  // logout user
  logout(): any {
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    localStorage.removeItem('role');
    localStorage.removeItem('name');
    localStorage.removeItem('profile');
    this.loginObservable.next({});
  }

  // fetch all users
  getAllUsers(): any {
    return this.http.get(`${this.userUrl}/allusers`);
  }

  // updating blockStatus
  updateBlockState(blockState, id): any {
    return this.http.put(`${this.userUrl}/updateblock/${id}`, blockState);
  }

  // updating onlineStatus
  updateOnlineStatus(onlineState, id): any {
    return this.http.put(
      `${this.userUrl}/updateonlinestatus/${id}`,
      onlineState
    );
  }

  // fetching no. of active users
  getActiveUsers(): any {
    return this.http.get(`${this.userUrl}/activeusers`);
  }

  // sahring file to a friend
  shareFile(fileId, email: any): any {
    return this.http.post(`${this.userUrl}/sharefile/${fileId}`, email);
  }

  // fetching shared file
  fetchSharedFile(): any {
    return this.http.get(`${this.userUrl}/sharedfiles`);
  }
}

interface LoginResponse {
  message: string;
  token: string;
  role: string;
  id: string;
  username: string;
  profile: string;
}
