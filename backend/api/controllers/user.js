const ErrorResponse = require("../../utils/errorResponse");
const User = require("../models/User");
const File = require("../models/File");
const nodemailer = require("nodemailer");

// Create Single User
exports.createUser = async (req, res, next) => {
  try {
    const user = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      profile: req.file.path,
    };
    const createdUser = await User.create(user);
    const emailToken = createdUser.getSignedJwtToken();
    // console.log(emailToken);
    const url = `http://localhost:3000/confirmation/${emailToken}`;

    // let testAccount = await nodemailer.createTestAccount();
    // let transporter = nodemailer.createTransport({
    //   host: "smtp.mailtrap.io",
    //   auth: {
    //     user: "85e4bc49ccb24a",
    //     pass: "db01923d832292",
    //   },
    // });
    let transporter = nodemailer.createTransport({
      service: "gmail",
      host: "smtp.gmail.com",
      port: "465",
      ssl: true,
      auth: {
        user: process.env.username,
        pass: process.env.password,
      },
    });
    await transporter.sendMail({
      from: transporter.user,
      to: req.body.email,
      subject: "Confirm Email",
      html: `Please click this link to confirm your email: <a href="${url}">${url}</a>`,
    });
    res.status(201).json({
      msg: "User Created Successfully",
    });
  } catch (err) {
    res.status(500).json({ msg: "Something went wrong"});
    return next(
      new ErrorResponse(`Something went wrong!! ${err.message}`, 500)
    );
  }
};

// login User
exports.loginUserAndAdmin = async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;

    if (!email || !password) {
      res.status(404).json({ msg: "Please Provide Email and Password"});
      return next(new ErrorResponse("Please Provide Email and Password", 400));
    }

    // check for user
    const user = await User.findOne({ email });
    // console.log(user);

    if (!user) {
      res.status(404).json({ msg: "User is not registered"});
      return next(new ErrorResponse("User is not registered", 401));
    }
    if (!user.verifiedUser) {
      res.status(404).json({ msg: "Please verify your email to login"});
      return next(new ErrorResponse("Please verify your email to login", 401));
    }

    // Check if password matches
    const isMatch = await user.matchPassword(password);

    if (!isMatch) {
      res.status(404).json({ msg: "Invalid credentials"});
      return next(new ErrorResponse("Invalid Credentials", 401));
    }

    const token = user.getSignedJwtToken();

    res.status(200).json({
      msg: "User logged in",
      token,
      id: user._id,
      role: user.role,
      username: user.username,
      profile: user.profile,
      blockedStatus: user.blockedStatus,
    });
  } catch (err) {
    res.status(500).json({ msg: "Something went wrong"});
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};

// get All users
exports.getAllUsers = async (req, res, next) => {
  try {
    const users = await User.find({
      role: "user",
      _id: { $ne: req.userData.id },
    });
    if (!users) {
      return next(new ErrorResponse(`No users`, 404));
    }
    // console.log(users);
    res.status(200).json(users);
  } catch (err) {
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};

// update block state
exports.updateBlock = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return next(new ErrorResponse(`No user`, 404));
    }
    user.blockedStatus = user.blockedStatus ? "false" : "true";
    await User.findByIdAndUpdate(req.params.id, {
      blockedStatus: user.blockedStatus,
    });
    res.status(200).json(user);
  } catch (err) {
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};

// update online status
exports.updateOnlineStatus = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return next(new ErrorResponse(`No user`, 404));
    }
    user.onlineStatus = user.onlineStatus ? "false" : "true";
    await User.findByIdAndUpdate(req.params.id, {
      onlineStatus: user.onlineStatus,
    });
    res.status(200).json(user);
  } catch (err) {
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};

// get Active Users
exports.getActiveUsers = async (req, res, next) => {
  try {
    const users = await User.find({ onlineStatus: true });
    if (!users) {
      return next(new ErrorResponse(`No user`, 404));
    }
    res.status(200).json(users);
  } catch (err) {
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};

exports.shareFile = async (req, res, next) => {
  try {
    const friendEmail = req.body.email;
    const fileID = req.params.id;
    const user = await User.findById(req.userData.id);
    // check if file belongs to user or not
    if (!user.files.includes(fileID)) {
      return next(new ErrorResponse(`File doesn't belongs to user`, 401));
    }

    const userToRecieveFile = await User.findOne({
      email: friendEmail,
    });
    if (!userToRecieveFile) {
      return next(new ErrorResponse(`Reciever User doesn't Exist`, 401));
    }

    const fileToBeShared = await File.findByIdAndUpdate(
      fileID,
      { sharedto: [userToRecieveFile._id] },
      {
        runValidators: true,
        new: true,
      }
    );

    userToRecieveFile.sharedFile.push(fileToBeShared.filename);

    await User.findOneAndUpdate(
      { email: req.body.email },
      {
        sharedFile: userToRecieveFile.sharedFile,
      }
    );
    res.status(200).json({ msg: `File shared successfully!!` });
  } catch (err) {
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};

exports.fetchSharedFile = async (req, res, next) => {
  try {
    const user = await User.findById(req.userData.id);
    if (!user) {
      return next(new ErrorResponse(`No user`, 404));
    }
    res.status(200).json(user.sharedFile);
  } catch (err) {
    return next(new ErrorResponse(`${err.message}`, 500));
  }
};
