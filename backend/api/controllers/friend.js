const User = require("../models/User");

// sending friend request
exports.addToFriendList = async (req, res, next) => {
  try {
    const requestFrom = await User.findById(req.userData.id);
    const requestTo = await User.findById(req.params.id);

    if (!requestFrom || !requestTo) {
      return res.status(500).json({ msg: "Something went wrong" });
    }

    //Check if request is previously sent or not
    const requestfound = await requestFrom.sentRequest.includes(req.params.id);
    if (requestfound) {
      return res.status(403).json({ msg: "Friend request already sent" });
    }

    requestFrom.sentRequest.push(req.params.id);
    requestTo.incomingRequest.push(req.userData.id);

    await User.findByIdAndUpdate(req.userData.id, {
      sentRequest: requestFrom.sentRequest,
    });

    await User.findByIdAndUpdate(req.params.id, {
      incomingRequest: requestTo.incomingRequest,
    });

    res.status(200).json({ msg: "Friend request sent" });
  } catch (err) {
    return res.status(500).json({ msg: "Something went wrong" });
  }
};

// fetching notifications
exports.notifications = async (req, res, next) => {
  try {
    const populate = [
      { path: "incomingRequest._id", select: "email _id username" },
    ];
    const user = await User.findById(req.userData.id).populate(populate);

    res.status(200).json({ incomingRequest: user.incomingRequest });
  } catch (err) {
    res.status(500).json({ msg: `Something went wrong ${err.message}` });
  }
};

// accepting requests
exports.acceptRequest = async (req, res, next) => {
  try {
    const toUser = await User.findById(req.userData.id);
    const fromUser = await User.findById(req.params.id);
    let index;
    
    for (i = 0; i < toUser.incomingRequest.length; i++) {
      if (toUser.incomingRequest[i]._id == req.params.id) {
        index = i;
        toUser.incomingRequest.splice(index, 1);
      }
    }
    toUser.friendList.push({ _id: req.params.id});
    
    await User.findByIdAndUpdate(req.userData.id, {
      friendList: toUser.friendList,
    });

    fromUser.friendList.push({ _id: req.userData.id});

    await User.findByIdAndUpdate(req.params.id, {
      friendList: fromUser.friendList,
    });

    toUser.sentRequest.push(req.params.id);

    await User.findByIdAndUpdate(req.userData.id, {
      sentRequest: toUser.sentRequest,
    });

    await User.findByIdAndUpdate(req.userData.id, {
      incomingRequest: toUser.incomingRequest,
    });

    res.status(200).json({ msg: 'Accepted' });
  } catch (err) {
    res.status(500).json({ msg: `Something went wrong ${err.message}` });
  }
};

// deleting requests
exports.rejectRequest = async (req, res, next) => {
  try {
    const user = await User.findById(req.userData.id);
    let index;

    for (i = 0; i < user.incomingRequest.length; i++) {
      if (user.incomingRequest[i]._id === req.params.id) {
        index = i;
      }
    }

    user.incomingRequest.splice(index, 1);

    await User.findByIdAndUpdate(req.userData.id, {
      incomingRequest: user.incomingRequest,
    });
    res.status(200).json({ msg: 'Rejected' });
  } catch (err) {
    res.status(500).json({ msg: `Something went wrong ${err.message}` });
  }
};

// fetching friends
exports.fetchFriends = async (req, res, next) => {
  try {
    const populate = [
      { path: "friendList._id", select: "email _id username profile" },
    ];
    const user = await User.findById(req.userData.id).populate(populate);

    res.status(200).json(user.friendList);
  } catch (err) {
    res.status(500).json({ msg: `Something went wrong ${err.message}` });
  }
};