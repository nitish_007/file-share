const express = require("express");
const router = express.Router();
const Auth = require("../middleware/check-auth");
const friendController = require("../controllers/friend");

router.put(
  "/addtofriendlist/:id",
  Auth.checkAuth,
  friendController.addToFriendList
);
router.get("/getfriendlist", Auth.checkAuth, friendController.fetchFriends);
router.get("/notifications", Auth.checkAuth, friendController.notifications);
router.put(
  "/acceptrequest/:id",
  Auth.checkAuth,
  friendController.acceptRequest
);
router.put(
  "/rejectrequest/:id",
  Auth.checkAuth,
  friendController.rejectRequest
);

module.exports = router;
