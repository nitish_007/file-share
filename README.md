A Fileshare app developed on MEAN Stack.
# FileShare

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.6.

## Development server

Run `ng serve` in frontend and then `npm run dev` in backend to start server. Then, Navigate to `http://localhost:4200/`.

## ADMIN Credentials
email: nitish@mail.com 
password: 123

## USER Credentials
email: nick@mail.com
password: 123

Author: 'Nitish Kumar'